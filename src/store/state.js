export default {
  user: {
    username: null,
    email: null,
  },
  appToken: null,
  productList: [],
};
