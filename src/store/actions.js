import { client } from '../providers/graphql';
import * as gqlQueries from './gqlQueries';
import * as gqlMutations from './gqlMutations';

export default {
  async login({ commit }, data) {
    const response = await client.mutate({
      mutation: gqlMutations.default.login,
      variables: data,
    });
    commit('setAppToken', response.data.login);
  },
  async getCurrentUser({ commit }) {
    const response = await client.query({
      query: gqlQueries.default.getCurrentUser,
    });
    commit('setCurrentUser', response.data.getCurrentUser);
  },
  async getAllProducts({ commit }) {
    const reponse = await client.query({
      query: gqlQueries.default.getAllProducts,
    });
    commit('setProductList', reponse.data.getAllProducts);
  },
};
