import gql from 'graphql-tag';

const login = gql`
  mutation Login($username: String!, $password: String!) {
    login(username: $username, password: $password)
  }
`;

const deleteProduct = gql`
  mutation DeleteProduct($_id: ID!) {
    deleteProduct(_id: $_id)
  }
`;

export default {
  login,
  deleteProduct,
};
