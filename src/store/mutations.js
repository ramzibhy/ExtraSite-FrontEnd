export default {
  setCurrentUser(state, user) {
    state.user = user;
  },
  setAppToken(state, appToken) {
    state.appToken = appToken;
  },
  setProductList(state, productList) {
    state.productList = productList;
  },
};
