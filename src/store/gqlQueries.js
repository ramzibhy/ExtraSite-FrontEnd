import gql from 'graphql-tag';

const getCurrentUser = gql`
  query {
    getCurrentUser {
      _id
      username
      firstName
      lastName
      email
      privilege
    }
  }
`;

const getAllProducts = gql`
  query {
    getAllProducts {
      _id
      name
      description
      picture
      price
    }
  }
`;

export default {
  getCurrentUser,
  getAllProducts,
};
