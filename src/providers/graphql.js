import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import { InMemoryCache } from 'apollo-cache-inmemory';
import store from '../store';

const httpLink = createHttpLink({
  uri: 'https://fantasia-shop.ddns.net/api/graphql',
});

const authLink = setContext((parent, { headers }) => ({
  headers: {
    ...headers,
    authorization: store.state.appToken ? `Bearer ${store.state.appToken}` : '',
  },
}));

const client = new ApolloClient({
  link: authLink.concat(httpLink),
  cache: new InMemoryCache(),
});
/* eslint-disable */
export { client };
