// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
// import Vuetify from 'vuetify';
import Buefy from 'buefy';
import VeeValidate from 'vee-validate';
// import 'vuetify/dist/vuetify.min.css';
import 'buefy/lib/buefy.css';
import '@mdi/font/css/materialdesignicons.min.css';
import App from './App';
import router from './router';
import store from './store';

router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  next();
});

Vue.config.productionTip = false;
Vue.config.devtools = false;
// Vue.use(Vuetify);
Vue.use(VeeValidate);
Vue.use(Buefy);
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});
