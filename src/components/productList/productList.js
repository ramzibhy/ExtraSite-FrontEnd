import { mapState } from 'vuex';

export default {
  name: 'product-list',
  components: {},
  props: [],
  data() {
    return {
      pageNumber: 0,
      productPerPage: 18,
      products: [],
    };
  },
  computed: {
    ...mapState(['productList']),
  },
  async beforeCreate() {
    await this.$store.dispatch('getAllProducts');
    this.products = JSON.parse(JSON.stringify(this.$store.state.productList));
  },
  created() {

  },
  mounted() {

  },
  methods: {
    nextPage() {
      const _this = this;
      if (this.pageNumber < this.pageCount()) {
        document.getElementById('generatedListPortion').classList = 'columns is-mobile is-multiline nextPage';
        document.getElementById('pageNavigationBtns').classList = 'has-text-centered disabledDiv';
        setTimeout(() => {
          _this.pageNumber += 1;
        }, 750);
        setTimeout(() => {
          document.getElementById('generatedListPortion').classList = 'columns is-mobile is-multiline';
          document.getElementById('pageNavigationBtns').classList = 'has-text-centered';
        }, 1500);
      }
    },
    prevPage() {
      const _this = this;
      if (this.pageNumber > 0) {
        document.getElementById('generatedListPortion').classList = 'columns is-mobile is-multiline predPage';
        document.getElementById('pageNavigationBtns').classList = 'has-text-centered disabledDiv';
        setTimeout(() => {
          _this.pageNumber -= 1;
        }, 750);
        setTimeout(() => {
          document.getElementById('generatedListPortion').classList = 'columns is-mobile is-multiline';
          document.getElementById('pageNavigationBtns').classList = 'has-text-centered';
        }, 1500);
      }
    },
    pageCount() {
      const l = this.products.length;
      return Math.floor(l / this.productPerPage);
    },
    paginatedData() {
      const start = this.pageNumber * this.productPerPage;
      const end = start + this.productPerPage;
      return this.products.slice(start, end);
    },
    showImage(title, description, price, picture) {
      this.$modal.open(
        `<div class="box">
          <div class="card">
            <div class="card-image">
                <figure class="image">
                    <img src="${picture}" alt="Image">
                </figure>
            </div>
            <div class="card-content">
              <div class="media">
                <div class="media-content">
                  <p class="title is-4" style="font-family: 'Amaranth'; font-size: 40px;">${title}</p>
                  <p class="subtitle is-6" style="font-family: 'Galada'; font-size: 30px;">Price: ${price}€</p>
                </div>
              </div>

              <div class="content" style="font-family: 'Reem Kufi'; font-size: 23px;">
                ${description}
              </div>
            </div>
          </div>
        </div>`,
      );
    },
  },
};

