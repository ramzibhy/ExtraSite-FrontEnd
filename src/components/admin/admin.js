/* eslint-disable */
import { mapState } from 'vuex';
import adminNavBar from '@/components/adminNavBar';

export default {
  name: 'admin',
  components: {
    'admin-nav-bar': adminNavBar,
  },
  props: [],
  data() {
    return {

    };
  },
  computed: {
    ...mapState(['user']),
  },
  mounted() {

  },
  beforeCreate() {
    this.$store.dispatch('getCurrentUser').then(() => {
      if (this.$store.state.user.username) {
        // console.log('welcome');
      }
    }).catch((err) => {
      // console.log(err);
      this.$router.push({ path: '/intro' });
    });
  },
  methods: {

  },
};
