import { mapState } from 'vuex';

export default {
  name: 'login',
  components: {},
  props: [],
  data() {
    return {

    };
  },
  computed: {
    ...mapState(['user']),
  },
  mounted() {

  },
  methods: {
    login() {
      const username = document.getElementById('username').value;
      const password = document.getElementById('password').value;
      this.$store.dispatch('login', { username, password }).then(() => {
        this.$router.push({ path: '/admin' });
      }).catch((err) => {
        // console.log(err);
        this.$toast.open({
          duration: 2500,
          message: `${err.message.replace('GraphQL error: ', '')}`,
          position: 'is-top',
          type: 'is-danger',
        });
      });
    },
  },
};

