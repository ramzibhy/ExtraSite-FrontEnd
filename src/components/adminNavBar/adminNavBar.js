export default {
  name: 'admin-nav-bar',
  components: {},
  props: [],
  data() {
    return {
      showNav: false,
      navElements: ['Create', 'Delete'],
    };
  },
  computed: {

  },
  mounted() {

  },
  methods: {
    goToCreate() {
      this.$router.push({ path: '/admin' });
    },
    goToDelete() {
      this.$router.push({ path: '/admin/delete_product' });
    },
  },
};

