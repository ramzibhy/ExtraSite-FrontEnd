import axios from 'axios';
import { client } from '../../providers/graphql';

export default {
  name: 'admin-add-product',
  components: {
  },
  props: [],
  data() {
    return {
      files: [],
      alert: true,
      title: 'Image Upload',
      dialog: false,
      dialogTitle: '',
      imageName: 'Choose the Image',
      imageUrl: '',
      imageFile: '',
      valid: true,
      name: '',
      description: '',
      price: '',
      nameRules: [
        v => !!v || 'Name is required',
        v => (v && v.length <= 25) || 'Name must be less than 25 characters',
      ],
      descriptionRules: [
        v => !!v || 'Description is required',
        v => (v && v.length <= 250) || 'Name must be less than 250 characters',
      ],
      priceRules: [
        v => !!v || 'Price is required',
        v => /^[0-9]+$/.test(v) || 'Price must be valid',
      ],
    };
  },
  computed: {

  },
  mounted() {

  },
  watch: {
    files() {
      this.onFilePicked(this.files);
    },
  },
  methods: {
    pickFile() {
      this.$refs.image.click();
    },
    onFilePicked(files) {
      if (files[0] !== undefined) {
        this.imageName = files[0].name;
        if (this.imageName.lastIndexOf('.') <= 0) {
          return;
        }
        const fr = new FileReader();
        fr.readAsDataURL(files[0]);
        fr.addEventListener('load', () => {
          this.imageUrl = fr.result;
          this.imageFile = files[0]; // this is an image file that can be sent to server...
        });
      }/* else {
        this.imageName = '';
        this.imageFile = '';
        this.imageUrl = '';
      } */
    },
    /*
    submit() {
      if (this.$refs.form.validate()) {
        // Native form submission is not yet supported
        if (this.$refs.form.validate()) {
          // Native form submission is not yet supported
          const bodyFormData = new FormData();
          bodyFormData.set('name', this.name);
          bodyFormData.set('description', this.description);
          bodyFormData.set('picture', this.imageFile);
          bodyFormData.set('price', this.price);
          axios({
            url: 'https://fantasia-shop.ddns.net/api/upload',
            data: bodyFormData,
            method: 'post',
            config: { headers: { 'Content-Type': 'multipart/form-data' } },
          }).then(() => {
            // handle success
            this.dialog = true;
            this.dialogTitle = 'Product Added';
            this.clear();
          }).catch(() => {
            // handle error
            this.dialog = true;
            this.dialogTitle = 'Error, Product wasnt added';
            this.clear();
          });
        }
      }
    },
    */
    clear() {
      this.name = '';
      this.description = '';
      this.price = '';
      this.files = [];
      this.imageName = 'Choose the Image';
      this.imageUrl = '';
      this.imageFile = '';
    },
    validateBeforeSubmit() {
      this.$validator.validateAll().then((result) => {
        if (result) {
          const bodyFormData = new FormData();
          bodyFormData.set('name', this.name);
          bodyFormData.set('description', this.description);
          bodyFormData.set('picture', this.imageFile);
          bodyFormData.set('price', this.price);
          axios({
            url: 'https://fantasia-shop.ddns.net/api/upload',
            data: bodyFormData,
            method: 'post',
            config: { headers: { 'Content-Type': 'multipart/form-data' } },
          }).then(() => {
            // handle success
            this.$toast.open({
              duration: 2500,
              message: 'Product Added',
              position: 'is-bottom',
              type: 'is-success',
            });
          }).catch(() => {
            // handle error
            this.$toast.open({
              duration: 2500,
              message: 'Error, Product wasnt added',
              position: 'is-bottom',
              type: 'is-danger',
            });
          });
          this.clear();
          client.resetStore();
          return;
        }
        this.$toast.open({
          duration: 3000,
          message: 'Fill the missing parts before submitting',
          position: 'is-bottom',
          type: 'is-warning',
        });
      });
    },
  },
};

