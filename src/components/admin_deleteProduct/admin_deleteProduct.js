import { mapState } from 'vuex';
import { client } from '../../providers/graphql';
import * as gqlMutations from '../../store/gqlMutations';
import * as gqlQueries from '../../store/gqlQueries';

export default {
  name: 'admin-delete-product',
  components: {},
  props: [],
  data() {
    return {
      products: [],
      columns: [
        {
          field: 'name',
          label: 'Name',
        },
        {
          field: 'description',
          label: 'Description',
        },
        {
          field: 'price',
          label: 'Price',
        },
      ],
    };
  },
  computed: {
    ...mapState(['productList']),
  },
  async beforeCreate() {
    await this.$store.dispatch('getAllProducts');
    this.products = JSON.parse(JSON.stringify(this.$store.state.productList));
  },
  mounted() {

  },
  methods: {
    async deleteProduct(_id) {
      const response = await client.mutate({
        mutation: gqlMutations.default.deleteProduct,
        variables: {
          _id,
        },
        refetchQueries: [{
          query: gqlQueries.default.getAllProducts,
        }],
      });
      this.$toast.open({
        duration: 2500,
        message: `${response.data.deleteProduct}`,
        position: 'is-bottom',
        type: 'is-success',
      });
      for (let i = 0; i < this.products.length; i += 1) {
        if (this.products[i]._id === _id) {
          this.products.splice(i, 1);
          break;
        }
      }
    },
  },
};

