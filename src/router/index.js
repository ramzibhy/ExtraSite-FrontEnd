import Vue from 'vue';
import Router from 'vue-router';
import Store from '@/components/store';
import Admin from '@/components/admin';
import Login from '@/components/login';
// import SignUp from '@/components/signup';
import Intro from '@/components/intro';
import AddProduct from '@/components/admin_addProduct';
import ProductList from '@/components/productList';
import DeleteProduct from '@/components/admin_deleteProduct';
import PageNotFound from '@/components/pageNotFound';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/intro',
      // name: 'Intro', because we have a root chile '/'
      component: Intro,
      children: [
        {
          path: '/',
          name: 'Login',
          meta: {
            title: 'Login to management',
          },
          component: Login,
        }, /*
        {
          path: '/signup',
          name: 'SignUp',
          component: SignUp,
        }, */
      ],
    },
    {
      path: '/admin',
      // name: 'Admin',
      component: Admin,
      children: [
        {
          path: '/',
          name: 'AddProduct',
          meta: {
            title: 'Admin Console',
          },
          component: AddProduct,
        },
        {
          path: 'delete_product',
          name: 'DeleteProduct',
          meta: {
            title: 'Admin Console',
          },
          component: DeleteProduct,
        },
      ],
    },
    {
      path: '/',
      // name: 'Store',
      component: Store,
      children: [
        {
          path: '/',
          name: 'ProductList',
          meta: {
            title: '❀ Fantasia Shop ❀',
          },
          component: ProductList,
        },
      ],
    },
    {
      path: '*',
      name: 'PageNotFound',
      meta: {
        title: '404 Not Found :\'(',
      },
      component: PageNotFound,
    },
  ],
});
